<?php

use Webman\Route;

Route::group('/api', function() {

    Route::any('/618e1ea855352',[\app\api\controller\Cps::class,'index'])->middleware([\app\middleware\ApiAuth::class, \app\middleware\ApiPermission::class, \app\middleware\ApiLog::class]);
});
