<?php

use Webman\Route;
use app\admin\controller\MysqlField;

Route::group('/admin', function() {

    Route::any('/MysqlField/index', [MysqlField::class,'index'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
});
