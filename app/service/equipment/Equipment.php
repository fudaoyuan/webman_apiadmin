<?php


namespace app\service\equipment;


class Equipment
{

    private static $return_array = array(); // 返回带有MAC地址的字串数组
    private static $mac_addr;

    /**
     * 电脑Mac地址获取
     * @param string $os_type
     * @return mixed
     */
    public static function GetMacAddr($os_type=PHP_OS)
    {
        switch ( strtolower($os_type) )
        {
            case "linux":
                self::forLinux();
                break;
            case "solaris":
                break;
            case "unix":
                break;
            case "aix":
                break;
            default:
                self::forWindows();
                break;
        }
        $temp_array = array();
        foreach ( self::$return_array as $value )
        {
            if ( preg_match( "/[0-9a-f][0-9a-f][:-]"."[0-9a-f][0-9a-f][:-]"."[0-9a-f][0-9a-f][:-]"."[0-9a-f][0-9a-f][:-]"."[0-9a-f][0-9a-f][:-]"."[0-9a-f][0-9a-f]/i", $value, $temp_array ) )
            {
                self::$mac_addr = $temp_array[0];
                break;
            }
        }
        unset($temp_array);
        return self::$mac_addr;
    }

    /**
     * 获取Windows下的mac地址
     * @return array
     */
    public static function forWindows()
    {
        @exec("ipconfig /all", self::$return_array);
        if ( self::$return_array )
            return self::$return_array;
        else{
            $ipconfig = $_SERVER["WINDIR"]."\system32\ipconfig.exe";
            if ( is_file($ipconfig) )
                @exec($ipconfig." /all", self::$return_array);
            else
                @exec($_SERVER["WINDIR"]."\system\ipconfig.exe /all", self::$return_array);
            return self::$return_array;
        }
    }

    /**
     * 获取Linux下的mac地址
     * @return array
     */
    public static function forLinux() {
        @exec ( "ifconfig -a", self::$return_array );
        return self::$return_array;
    }

}
