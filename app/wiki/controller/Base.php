<?php
declare (strict_types=1);
/**
 * 工程基类
 */

namespace app\wiki\controller;

use app\BaseController;
use app\util\ReturnCode;
use support\Response;

class Base extends BaseController {

    public function buildSuccess($data = [], $msg = '操作成功', $code = ReturnCode::SUCCESS): Response {
        $return = [
            'code' => $code,
            'msg'  => $msg,
            'data' => $data
        ];

        return json($return);
    }

    public function buildFailed($code, $msg = '操作失败', $data = []): Response {
        $return = [
            'code' => $code,
            'msg'  => $msg,
            'data' => $data
        ];

        return json($return);
    }
}
