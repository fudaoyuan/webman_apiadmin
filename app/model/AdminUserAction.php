<?php
/**
 *
 * @since   2018-02-11
 * @author  zhaoxiang <zhaoxiang051405@gmail.com>
 */

namespace app\model;

class AdminUserAction extends Base
{

    protected $table = "admin_user_action";
}
