<?php
declare (strict_types=1);

namespace app\api\controller;

use support\Request;
use think\Exception;

use support\Response;
class Miss extends Base {

    public function index(Request $request): Response {
        $version = config('apiwebman.APP_VERSION');
        if ($version) {
            return $this->buildSuccess([
                'Product'    => config('apiwebman.APP_NAME'),
                'ApiVersion' => $version,
                'TpVersion'  => $request->protocolVersion(),
                'Company'    => config('apiwebman.COMPANY_NAME'),
                'ToYou'      => "I'm glad to meet you（终于等到你！）"
            ]);
        }
    }
}
