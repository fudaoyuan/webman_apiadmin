<?php
declare (strict_types=1);
/**
 * 登录登出
 * @since   2017-11-02
 * @author  bubaishaolong <584887013@qq.com>
 */

namespace app\admin\controller;

use app\model\AdminAuthGroupAccess;
use app\model\AdminAuthRule;
use app\model\AdminMenu;
use app\model\AdminUser;
use app\model\AdminUserData;
use app\util\ReturnCode;
use app\util\RouterTool;
use app\util\Tools;
use support\Response;

class Login extends Base {

    /**
     * 用户登录【账号密码登录】
     * @return Response
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @author bubaishaolong <584887013@qq.com>
     */
    public function index(): Response {
        $username =request()->post('username');
        $password =request()->post('password');
        if (!$username) {
            return $this->buildFailed(ReturnCode::LOGIN_ERROR, '缺少用户名!');
        }
        if (!$password) {
            return $this->buildFailed(ReturnCode::LOGIN_ERROR, '缺少密码!');
        } else {
            $password = Tools::userMd5($password);
        }
        $userInfo = (new AdminUser())->where('username', $username)->where('password', $password)->find();
        if (!empty($userInfo)) {
            if ($userInfo['status']) {
                //更新用户数据
                $userData = $userInfo->userData;
                $data = [];
                if ($userData) {
                    $userData->login_times++;
                    $userData->last_login_ip = sprintf("%u", ip2long(request()->getRealIp(true)));
                    $userData->last_login_time = time();
                    $userData->save();
                } else {
                    $data['login_times'] = 1;
                    $data['uid'] = $userInfo['id'];
                    $data['last_login_ip'] = sprintf("%u", ip2long(request()->getRealIp(true)));
                    $data['last_login_time'] = time();
                    $data['head_img'] = '';
                    AdminUserData::create($data);

                    $userInfo['userData'] = $data;
                }
            } else {
                return $this->buildFailed(ReturnCode::LOGIN_ERROR, '用户已被封禁，请联系管理员');
            }
        } else {
            return $this->buildFailed(ReturnCode::LOGIN_ERROR, '用户名密码不正确');
        }
        $userInfo_data = $userInfo->toArray();
        $userInfo_data['access'] = $this->getAccess($userInfo['id']);
        $userInfo_data['menu'] = $this->getAccessMenuData($userInfo['id']);
        $apiAuth = md5(uniqid() . time());
        cache('Login' . $apiAuth, json_encode($userInfo_data), config('apiwebman.ONLINE_TIME'));
        cache('Login' . $userInfo['id'], $apiAuth, config('apiwebman.ONLINE_TIME'));
        $userInfo_data['apiAuth'] = $apiAuth;
        return $this->buildSuccess($userInfo_data, '登录成功');
    }

    /**
     * 获取用户信息
     * @return Response
     * @author bubaishaolong <584887013@qq.com>
     */
    public function getUserInfo(): Response {
        $request =request();
        $ApiAuth = $request->header('Api-Auth');
        $userInfo_cache = cache('Login' . $ApiAuth);
        $userInfo = [];
        if ($userInfo_cache) {
            $userInfo = json_decode($userInfo_cache, true);
        }
        return $this->buildSuccess($userInfo,'获取成功');
    }

    /**
     * 用户登出
     * @return Response
     * @author bubaishaolong <584887013@qq.com>
     */
    public function logout(): Response {
        $request =request();
        $ApiAuth = $request->header('Api-Auth');
        if ($ApiAuth) {
            $userInfo = cache('Login' . $ApiAuth);
            if ($userInfo) {
                $userInfo = json_decode($userInfo, true);
                if($userInfo){
                    cache('Login' . $userInfo['id'], null);
                }
            }
        }
        session('admin_user_id','');
        cache('Login' . $ApiAuth, null);
        return $this->buildSuccess([], '登出成功');
    }

    /**
     * 获取当前用户的允许菜单
     * @return Response
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @author bubaishaolong <584887013@qq.com>
     */
    public function getAccessMenu(): Response {
        $request =request();
        $ApiAuth = $request->header('Api-Auth');
        $userInfo_cache = cache('Login' . $ApiAuth);
        $userInfo = json_decode($userInfo_cache, true);
        return $this->buildSuccess($this->getAccessMenuData($userInfo['id']));
    }

    /**
     * 获取当前用户的允许菜单
     * @param int $uid
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @author bubaishaolong <584887013@qq.com>
     */
    public function getAccessMenuData(int $uid): array {
        $returnData = [];
        $isSupper = Tools::isAdministrator($uid);
        if ($isSupper) {
            $access = (new AdminMenu())->where('router', '<>', '')->select();
            //$access = (new AdminMenu())->select();
            $returnData = Tools::listToTree(Tools::buildArrFromObj($access));
        } else {
            $groups = (new AdminAuthGroupAccess())->where('uid', $uid)->find();
            if (isset($groups) && $groups->group_id) {
                $access = (new AdminAuthRule())->whereIn('group_id', $groups->group_id)->select();
                $access = array_unique(array_column(Tools::buildArrFromObj($access), 'url'));
                array_push($access, "");
                $menus = (new AdminMenu())->whereIn('url', $access)->where('show', 1)->select();
                $returnData = Tools::listToTree(Tools::buildArrFromObj($menus));
                RouterTool::buildVueRouter($returnData);
            }
        }

        return array_values($returnData);
    }

    /**
     * 获取用户权限数据
     * @param $uid
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @author bubaishaolong <584887013@qq.com>
     */
    public function getAccess(int $uid): array {
        $isSupper = Tools::isAdministrator($uid);
        if ($isSupper) {
            $access = (new AdminMenu())->select();
            $access = Tools::buildArrFromObj($access);

            return array_values(array_filter(array_column($access, 'url')));
        } else {
            $groups = (new AdminAuthGroupAccess())->where('uid', $uid)->find();
            if (isset($groups) && $groups->group_id) {
                $access = (new AdminAuthRule())->whereIn('group_id', $groups->group_id)->select();
                $access = Tools::buildArrFromObj($access);

                return array_values(array_unique(array_column($access, 'url')));
            } else {
                return [];
            }
        }
    }
}
