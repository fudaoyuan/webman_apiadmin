<?php
declare (strict_types=1);
/**
 * 权限相关配置
 * @since   2018-02-06
 * @author  bubaishaolong <584887013@qq.com>
 */

namespace app\admin\controller;

use app\model\AdminAuthGroup;
use app\model\AdminAuthGroupAccess;
use app\model\AdminAuthRule;
use app\model\AdminMenu;
use app\util\ReturnCode;
use app\util\Tools;
use support\Response;

class Auth extends Base {

    /**
     * 获取权限组列表
     * @return Response
     * @throws \think\db\exception\DbException
     * @author bubaishaolong <584887013@qq.com>
     */
    public function index(): Response {
        $limit =request()->get('size', config('apiwebman.ADMIN_LIST_DEFAULT'));
        $start =request()->get('page', 1);
        $keywords =request()->get('keywords', '');
        $status =request()->get('status', '');

        $obj = new AdminAuthGroup();
        if (strlen($status)) {
            $obj = $obj->where('status', $status);
        }
        if ($keywords) {
            $obj = $obj->whereLike('name', "%{$keywords}%");
        }

        $listObj = $obj->order('id', 'DESC')->paginate(['page' => $start, 'list_rows' => $limit])->toArray();

        return $this->buildSuccess([
            'list'  => $listObj['data'],
            'count' => $listObj['total']
        ]);
    }

    /**
     * 获取全部已开放的可选组
     * @return Response
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @author bubaishaolong <584887013@qq.com>
     */
    public function getGroups(): Response {
        $listInfo = (new AdminAuthGroup())->where(['status' => 1])->order('id', 'DESC')->select();
        $count = count($listInfo);
        $listInfo = Tools::buildArrFromObj($listInfo);

        return $this->buildSuccess([
            'list'  => $listInfo,
            'count' => $count
        ]);
    }

    /**
     * 获取组所在权限列表
     * @return Response
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @author bubaishaolong <584887013@qq.com>
     */
    public function getRuleList(): Response {
        $groupId =request()->get('group_id', 0);

        $list = (new AdminMenu)->order('sort', 'ASC')->select();
        $list = Tools::buildArrFromObj($list);
        $list = Tools::listToTree($list);

        $rules = [];
        if ($groupId) {
            $rules = (new AdminAuthRule())->where(['group_id' => $groupId])->select();
            $rules = Tools::buildArrFromObj($rules);
            $rules = array_column($rules, 'url');
        }
        $newList = $this->buildList($list, $rules);

        return $this->buildSuccess([
            'list' => $newList
        ]);
    }

    /**
     * 新增组
     * @return Response
     * @author bubaishaolong <584887013@qq.com>
     */
    public function add(): Response {
        $res = AdminAuthGroup::create([
            'name'        =>request()->post('name', ''),
            'description' =>request()->post('description', '')
        ]);
        if ($res === false) {
            return $this->buildFailed(ReturnCode::DB_SAVE_ERROR);
        }

        return $this->buildSuccess();
    }

    /**
     * 权限组状态编辑
     * @return Response
     * @author bubaishaolong <584887013@qq.com>
     */
    public function changeStatus(): Response {
        $id =request()->get('id');
        $status =request()->get('status');
        $res = AdminAuthGroup::update([
            'id'     => $id,
            'status' => $status
        ]);
        if ($res === false) {
            return $this->buildFailed(ReturnCode::DB_SAVE_ERROR);
        }

        return $this->buildSuccess();
    }

    /**
     * 编辑用户
     * @return Response
     * @author bubaishaolong <584887013@qq.com>
     */
    public function edit(): Response {
        $res = AdminAuthGroup::update([
            'id'          =>request()->post('id', 0),
            'name'        =>request()->post('name', ''),
            'description' =>request()->post('description', '')
        ]);
        if ($res === false) {
            return $this->buildFailed(ReturnCode::DB_SAVE_ERROR);
        }

        return $this->buildSuccess();
    }

    /**
     * 删除组
     * @return Response
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @author bubaishaolong <584887013@qq.com>
     */
    public function del(): Response {
        $id =request()->get('id');
        if (!$id) {
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS, '缺少必要参数');
        }

        $listInfo = (new AdminAuthGroupAccess())->where('find_in_set("' . $id . '", `group_id`)')->select();
        if ($listInfo) {
            foreach ($listInfo as $value) {
                $oldGroupArr = explode(',', $value->group_id);
                $key = array_search($id, $oldGroupArr);
                unset($oldGroupArr[$key]);
                $newData = implode(',', $oldGroupArr);
                $value->group_id = $newData;
                $value->save();
            }
        }

        AdminAuthGroup::destroy($id);
        AdminAuthRule::destroy(['group_id' => $id]);

        return $this->buildSuccess();
    }

    /**
     * 从指定组中删除指定用户
     * @return Response
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @author bubaishaolong <584887013@qq.com>
     */
    public function delMember(): Response {
        $gid =request()->get('gid', 0);
        $uid =request()->get('uid', 0);
        if (!$gid || !$uid) {
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS, '缺少必要参数');
        }
        $oldInfo = (new AdminAuthGroupAccess())->where('uid', $uid)->find()->toArray();
        $oldGroupArr = explode(',', $oldInfo['group_id']);
        $key = array_search($gid, $oldGroupArr);
        unset($oldGroupArr[$key]);
        $newData = implode(',', $oldGroupArr);
        $res = AdminAuthGroupAccess::update([
            'group_id' => $newData
        ], [
            'uid' => $uid
        ]);
        if ($res === false) {
            return $this->buildFailed(ReturnCode::DB_SAVE_ERROR);
        }

        return $this->buildSuccess();
    }

    /**
     * 构建适用前端的权限数据
     * @param $list
     * @param $rules
     * @return array
     * @author bubaishaolong <584887013@qq.com>
     */
    private function buildList($list, $rules): array {
        $newList = [];
        foreach ($list as $key => $value) {
            $newList[$key]['title'] = $value['title'];
            $newList[$key]['key'] = $value['url'];
            if (isset($value['children'])) {
                $newList[$key]['expand'] = true;
                $newList[$key]['children'] = $this->buildList($value['children'], $rules);
            } else {
                if (in_array($value['url'], $rules)) {
                    $newList[$key]['checked'] = true;
                }
            }
        }

        return $newList;
    }

    /**
     * 编辑权限细节
     * @return Response
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @author bubaishaolong <584887013@qq.com>
     */
    public function editRule(): Response {
        $id =request()->post('id', 0);
        $rules =request()->post('rules', []);
        if (is_array($rules)) {
            $needAdd = [];
            $has = (new AdminAuthRule())->where(['group_id' => $id])->select();
            $has = Tools::buildArrFromObj($has);
            $hasRule = array_column($has, 'url');
            $needDel = array_flip($hasRule);
            foreach ($rules as $key => $value) {
                if (!empty($value)) {
                    if (!in_array($value, $hasRule)) {
                        $data['url'] = $value;
                        $data['group_id'] = $id;
                        $needAdd[] = $data;
                    } else {
                        unset($needDel[$value]);
                    }
                }
            }
            if (count($needAdd)) {
                (new AdminAuthRule())->saveAll($needAdd);
            }
            if (count($needDel)) {
                $urlArr = array_keys($needDel);
                (new AdminAuthRule())->whereIn('url', $urlArr)->where('group_id', $id)->delete();
            }
        }

        return $this->buildSuccess();
    }
}
