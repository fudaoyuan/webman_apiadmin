
## 特别鸣谢

web前端参考[ApiAdmin-Element](https://gitee.com/apiadmin/ApiAdmin-Element) 
静态文件也可以查看public下的[ApiAdmin-Element](https://gitee.com/bubaishaolong/webman_apiadmin/tree/master/public/ApiAdmin)

后端参考[apiadmin](https://gitee.com/apiadmin/ApiAdmin)

具体用法请移步[apiadmin官网](http://www.apiadmin.org/)

数据表在public目录下

采用laravelOrm的模型处理数据[位置](https://gitee.com/bubaishaolong/webman_apiadmin/tree/webman_laravel_apiadmin/)

登录默认的账号密码:admin 123456
# composer忽略平台以及扩展依赖强制安装

composer install --ignore-platform-reqs

composer update --ignore-platform-reqs

# 清除npm缓存,避免编译出错
npm cache clear --force
# 安装
npm install
# 运行
npm run dev

# webman
High performance HTTP Service Framework for PHP based on [Workerman](https://github.com/walkor/workerman).

# Manual
https://www.workerman.net/doc/webman

## LICENSE
MIT


