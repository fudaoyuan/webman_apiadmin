<?php

return [
    'annotations' => [
        'scan' => [
            'paths' => [
                BASE_PATH . '/app',
            ],
            'ignore_annotations' => [
                'mixin',
            ],
            'class_map' => [
            ],
        ],
    ],
    'aspects' => [
        // 这里写入对应的 Aspect
        //app\aspect\DebugAspect::class,
    ],
    // 表单公共配置
    'form_config'                           => [
        // 表单按钮配置
        'submitBtn'                         => [
            // 确定按钮配置
            'confirmShow'                   => true,
            // 确定按钮图标类名（支持element，HP图标类名）
            'confirmIconClass'              => 'el-icon-upload',
            // 确定按钮类型（element按钮type类型）
            'confirmType'                   => 'primary',
            // 确定按钮文字
            'confirmText'                   => '提 交',
            // 是否显示取消按钮
            'cancelShow'                    => true,
            // 取消按钮图标类名（支持element，HP图标类名）
            'cancelIconClass'               => 'el-icon-close',
            // 取消按钮类型（element按钮type类型）
            'cancelType'                    => 'danger',
            // 取消按钮文字
            'cancelText'                    => '取 消',
        ],
        // 表单布局配置
        'formUi'                              => [
            //行内表单模式
            'inline'                        => false,
            //表单域标签的位置，如果值为 left 或者 right 时，则需要设置 label-width
            'labelPosition'                 => 'left',
            //表单域标签的后缀
            'labelSuffix'                   => '',
            //是否显示必填字段的标签旁边的红色星号
            'hideRequiredAsterisk'          => true,
            //表单域标签的宽度，例如 '50px'。作为 Form 直接子元素的 form-item 会继承该值。支持 auto。
            'labelWidth'                    => 'auto',
            //是否显示校验错误信息
            'showMessage'                   => true,
            //是否以行内形式展示校验信息
            'inlineMessage'                 => false,
            //是否在输入框中显示校验结果反馈图标
            'statusIcon'                    => false,
            //是否在 rules 属性改变后立即触发一次验证
            'validateOnRuleChange'          => true,
            //是否禁用该表单内的所有组件。若设置为 true，则表单内组件上的 disabled 属性不再生效
            'disabled'                      => false,
            //用于控制该表单内组件的尺寸 medium / small / mini
            'size'                          => 'small',
        ],
        'formRow'                           => [
            //栅格间隔（单位PX）
            'gutter'                        => 20,
            //布局模式，可选 flex，现代浏览器下有效
            'type'                          => '',
            //flex 布局下的垂直排列方式 top/middle/bottom
            'align'                         => '',
            //flex 布局下的水平排列方式 start/end/center/space-around/space-between
            'justify'                       => '',
            //自定义元素标签
            'tag'                           => 'div'
        ],
    ],
];
