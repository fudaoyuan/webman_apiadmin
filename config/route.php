<?php

use app\middleware\WikiAuth;
use support\Request;
use Webman\Route;
use app\admin\controller\Login;
use app\admin\controller\Menu;
use app\admin\controller\User;
use app\admin\controller\Auth;
use app\admin\controller\App;
use app\admin\controller\InterfaceList;
use app\admin\controller\Fields;
use app\admin\controller\InterfaceGroup;
use app\admin\controller\AppGroup;
use app\admin\controller\Log;
use app\admin\controller\Index;
use app\admin\controller\Miss;

Route::group('/admin', function() {
    Route::any('/Login/index', [Login::class,'index']);
    Route::any('/Login/logout',  [Login::class,'logout'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminLog::class]);
    Route::any('/Menu/changeStatus', [Menu::class,'changeStatus'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/Menu/add', [Menu::class,'add'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/Menu/edit', [Menu::class,'edit'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/Menu/del', [Menu::class,'del'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/User/getUsers', [User::class,'getUsers'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/User/changeStatus', [User::class,'changeStatus'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/User/add', [User::class,'add'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/User/edit', [User::class,'edit'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/User/del', [User::class,'del'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/Auth/changeStatus',  [Auth::class,'changeStatus'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/Auth/delMember',  [Auth::class,'delMember'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/Auth/add',  [Auth::class,'add'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/Auth/edit',  [Auth::class,'edit'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/Auth/del',  [Auth::class,'del'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/Auth/getGroups',  [Auth::class,'getGroups'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/Auth/getRuleList',  [Auth::class,'getRuleList'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/App/changeStatus', [App::class,'changeStatus'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/App/getAppInfo', [App::class,'getAppInfo'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/App/add', [App::class,'add'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/App/edit', [App::class,'edit'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/App/del', [App::class,'del'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/InterfaceList/changeStatus', [InterfaceList::class,'changeStatus'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/InterfaceList/getHash', [InterfaceList::class,'getHash'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/InterfaceList/add', [InterfaceList::class,'add'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/InterfaceList/edit', [InterfaceList::class,'edit'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/InterfaceList/del', [InterfaceList::class,'del'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/Fields/request', [Fields::class,'request'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/Fields/response', [Fields::class,'response'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/Fields/add', [Fields::class,'add'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/Fields/upload', [Fields::class,'upload'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/Fields/edit', [Fields::class,'edit'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/Fields/del', [Fields::class,'del'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/InterfaceGroup/add', [InterfaceGroup::class,'add'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/InterfaceGroup/edit', [InterfaceGroup::class,'edit'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/InterfaceGroup/del', [InterfaceGroup::class,'del'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/InterfaceGroup/getAll', [InterfaceGroup::class,'getAll'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/InterfaceGroup/changeStatus', [InterfaceGroup::class,'changeStatus'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/AppGroup/add', [AppGroup::class,'add'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/AppGroup/edit', [AppGroup::class,'edit'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/AppGroup/del', [AppGroup::class,'del'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/AppGroup/getAll', [AppGroup::class,'getAll'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/AppGroup/changeStatus', [AppGroup::class,'changeStatus'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/Menu/index', [Menu::class,'index'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/User/index', [User::class,'index'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/Auth/index', [Auth::class,'index'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/App/index', [App::class,'index'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/AppGroup/index', [AppGroup::class,'index'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/InterfaceList/index', [InterfaceList::class,'index'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/InterfaceGroup/index', [InterfaceGroup::class,'index'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/Log/index', [Log::class,'index'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/Log/del', [Log::class,'del'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/InterfaceList/refresh', [InterfaceList::class,'refresh'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/Index/upload', [Index::class,'upload'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/User/own', [User::class,'own'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/App/refreshAppSecret', [App::class,'refreshAppSecret'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/Login/getUserInfo', [Login::class,'getUserInfo'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class]);
    Route::any('/Auth/editRule', [Auth::class,'editRule'])->middleware([\app\middleware\AdminAuth::class, \app\middleware\AdminPermission::class, \app\middleware\AdminLog::class]);
    Route::any('/Login/getAccessMenu', [Login::class,'getAccessMenu'])->middleware([\app\middleware\AdminAuth::class]);
    //接口异常处理
    Route::any('/Miss/index', [Miss::class,'index'])->middleware([\app\middleware\AdminAuth::class]);

});

include base_path().'/route/api.php';
include base_path().'/route/admin.php';
include base_path().'/route/home.php';

Route::group('/api',function (){
    Route::any('/get_access_token', [\app\api\controller\BuildToken::class,'getAccessToken']);
});

/**
 * 接口文档
 */
Route::group('/wiki', function() {
    Route::any('/Api/login', [\app\wiki\controller\Api::class,'login']);
    Route::group('/Api', function() {
       // Route::any('/login', [\app\wiki\controller\Api::class,'login']);
        Route::any('/errorCode', [\app\wiki\controller\Api::class,'errorCode']);
        Route::any('/groupList', [\app\wiki\controller\Api::class,'groupList']);
        Route::any('/detail', [\app\wiki\controller\Api::class,'detail']);
        Route::any('/logout', [\app\wiki\controller\Api::class,'logout']);
    })->middleware([\app\middleware\WikiAuth::class]);
});
Route::disableDefaultRoute();
