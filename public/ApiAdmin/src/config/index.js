export default {
  /**
   * @description 配置显示在浏览器标签的title
   */
  title: 'ApiWebman身边的接口管理专家',
  /**
   * @description 是否使用国际化，默认为false
   *              如果不使用，则需要在路由中给需要在菜单中展示的路由设置meta: {title: 'xxx'}
   *              用来在菜单中显示文字
   */
  useI18n: false,
  /**
   * @description api请求基础路径
   */
  baseUrl: {
    dev: 'http://127.0.0.1:8787/',
    pro: 'https://api.apiadmin.org/'
  },

  wsUrl:{
    dev: 'ws://127.0.0.1:2345',
    pro: 'ws://127.0.0.1:2345'
  },

  /**
   * @description 需要加载的插件
   */
  plugin: {

  }
}
